<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Index Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
    <br />
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
    <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Avatar</th>
        <th>Driver Name</th>
        <th>Driver Id</th>
        <th>Phone</th>
        <th colspan="2">Action</th>
      </tr>
    </thead>
    <tbody>
    <!--  -->
      @foreach($drivers as $drivers)      
      <tr>
        <td>{{$drivers->id}}</td>
        @if($drivers->avatar)
          <td><img src="{{asset('/')}}{{$drivers->avatar}}" width='50' height='50'></td>
        @else  
        <td></td>
        @endif
        <td>{{$drivers->name}}</td>
        <td>{{$drivers->driverId}}</td>
        <td>{{$drivers->phone}}</td>
        <td><a href="{{action('DriverController@edit', $drivers->id)}}" class="btn btn-warning">Edit</a></td>
        <td>
          <form action="{{action('DriverController@destroy', $drivers->id)}}" method="post">
          {{ csrf_field() }}
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
  </body>
</html>