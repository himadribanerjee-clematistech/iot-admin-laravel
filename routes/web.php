<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */

Route::get('/', 'DriverController@index');

Route::resource('drivers','DriverController@index');
Route::get('add','DriverController@create');
Route::post('store','DriverController@store');
Route::get('drivers','DriverController@index');
Route::get('edit/{id}','DriverController@edit');
Route::post('edit/{id}','DriverController@update');
Route::delete('{id}','DriverController@destroy');
