<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('cargodriver/all', 'API\CargoDriverController');
Route::get('cargodriver/edit/{id}','API\CargoDriverController@edit');
Route::post('cargodriver/add','API\CargoDriverController@store');

Route::post('cargodriver/update','API\CargoDriverController@update');
Route::get('cargodriver/destroy/{id}','API\CargoDriverController@destroy');

