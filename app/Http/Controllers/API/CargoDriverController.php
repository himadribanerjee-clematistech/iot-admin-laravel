<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Driver;
use Validator;

class CargoDriverController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        //$drivers = Driver::all();
        $drivers = Driver::orderBy('created_at', 'desc')->get();

        return $this->sendResponse($drivers->toArray(), 'Drivers retrieved successfully.');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'driverName' => 'required',
            'driverId' => 'required',
            'phone' => 'required',
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);


        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }


        $avatarInfo = $request->file('avatar');
        $avatarName = $avatarInfo->getClientOriginalName();
        
        $uploadFolder = 'avatarImage/';
        $avatarInfo->move($uploadFolder,$avatarName);
        $avatarURL = $uploadFolder.$avatarName;

        $driver = new Driver();
        $driver->name = $input['driverName'];
        $driver->driverId = $input['driverId'];
        $driver->phone = $input['phone'];  
        $driver->avatar = $avatarURL;      
        $driver->save();


        return $this->sendResponse($driver->toArray(), 'Driver created successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $driver = Driver::find($id);

        if (is_null($driver)) {
            return $this->sendError('Driver not found.');
        }

        return $this->sendResponse($driver->toArray(), 'Driver retrieved successfully.');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $input = $request->all();
        $id = $input['id'];  

        $validator = Validator::make($input, [
            'driverName' => 'required',
            'driverId' => 'required',
            'phone' => 'required',
        ]);


        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }


        $driver = Driver::find($id);

        if (is_null($driver)) {
            return $this->sendError('Driver not found.');
        }

        $avatarInfo = $request->file('avatar');
        if($avatarInfo)
        {
            $avatarName = $avatarInfo->getClientOriginalName();
        }
                

        $driver->name = $request->get('driverName');
        $driver->driverId = $request->get('driverId');
        $driver->phone = $request->get('phone');  
        
        if(isset($avatarName))
        {
            $uploadFolder = 'avatarImage/';
            $avatarInfo->move($uploadFolder,$avatarName);
            $avatarURL = $uploadFolder.$avatarName;

            $driver->avatar = $avatarURL; 
        }
        

        $driver->save();


        return $this->sendResponse($driver->toArray(), 'Driver updated successfully.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $driver = Driver::find($id);

        if (is_null($driver)) {
            return $this->sendError('Driver not found.');
        }

        //$driver->delete();

        return $this->sendResponse($driver->toArray(), 'Driver deleted successfully.');
    }
}