<?php

namespace App\Http\Controllers;
use App\Driver;
use Illuminate\Http\Request;


class DriverController extends Controller
{

    public function index()
    {
        $drivers=Driver::all();
        //$drivers = [];
        return view('driverindex',compact('drivers'));

        /* return view('driverindex',compact('drivers'))
            ->with('i', (request()->input('page', 1) - 1) * 5); */
        
    }

    public function create()
    {
        return view('drivercreate');
    }

    public function store(Request $request)
    {
        request()->validate([
            'driverName' => 'required',
            'driverId' => 'required',
            'phone' => 'required',
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        $avatarInfo = $request->file('avatar');
        $avatarName = $avatarInfo->getClientOriginalName();
        
        $uploadFolder = 'avatarImage/';
        $avatarInfo->move($uploadFolder,$avatarName);
        $avatarURL = $uploadFolder.$avatarName;

        $driver = new Driver();
        $driver->name = $request->get('driverName');
        $driver->driverId = $request->get('driverId');
        $driver->phone = $request->get('phone');  
        $driver->avatar = $avatarURL;      
        $driver->save();

        return redirect('drivers')->with('success', 'Driver has been successfully added');
    }

    public function edit($id)
    {
        $driver = Driver::find($id);
        return view('driveredit',compact('driver','id'));
    }

    public function update(Request $request, $id)
    {
        request()->validate([
            'name' => 'required',
            'driverId' => 'required',
            'phone' => 'required',
        ]);

        $driver= Driver::find($id);
        $driver->name = $request->get('driverName');
        $driver->driverId = $request->get('driverId');
        $driver->phone = $request->get('phone');        
        $driver->save();
        return redirect('drivers')->with('success', 'Driver has been successfully update');
    }

    public function destroy($id)
    {
        $driver = Driver::find($id);
        $driver->delete();
        return redirect('drivers')->with('success','Driver has been  deleted');
    }
}
