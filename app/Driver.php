<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Driver extends Eloquent
{
    //

    protected $connection = 'mongodb';
    protected $collection = 'driverdetails';

    protected $fillable = [
        'name', 'driverId','phone','avatar'
    ];
}
